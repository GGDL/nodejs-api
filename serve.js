
var fs = require('fs');
var path = require('path');
var filePathCampaigns = path.join(__dirname, 'campaigns.json');
var filePathFilters = path.join(__dirname, 'filters.json');
var filePathCards = path.join(__dirname, 'cards.json');
var express = require('express');
var cors = require('cors');
var app = express();


app.use(cors());

function sendJsonFile(filePath,req,res){
  req.is('application/json');
  res.header("Access-Control-Allow-Origin", "*");
  fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
  if (!err) {
    console.log(data);
     res.send(data);
   }
 });
}

app.get('/campaigns', function (req, res, next) {
  sendJsonFile(filePathCampaigns,req,res);
});

app.get('/cards', function (req, res, next) {
  sendJsonFile(filePathCards,req,res);
 });

 app.get('/filters', function (req, res, next) {
   sendJsonFile(filePathFilters,req,res);
  });

app.listen(8080, function (){
});
